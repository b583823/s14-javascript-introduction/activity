console.log("Hello World");

let user={
	firstName: "John",
	lastName: "Smith",
	age:30,
	hobbies:["Biking", "Mountain Climbing", "Swimming"],
	workAddress:{
		houseNumber: 32,
		street: "Washington",
		city: "Lincoln",
		state: "Nebraska"
	},
	isMarried: true
}
// console.log(user.firstName);
console.log("First Name: " + user.firstName);
console.log("Last Name: "+ user.lastName);
console.log("Age: "+ user.age);

function printUserInfo(userName, userLastName,userAge,userHobbies,work){
	console.log(userHobbies);
	console.log(work);
	console.log(userName+" "+userLastName+" is "+userAge+" years of age.");
	};
printUserInfo(user.firstName, user.lastName, user.age,user.hobbies,user.workAddress);

function returnFunction(hobbies) {
	return hobbies; 
};
let hob = returnFunction(user.hobbies);
console.log(hob);
function returnFunction1(address1){
	return address1
};
let add = returnFunction1(user.workAddress);
console.log(add);

function returnFunction2(civilStatus){
	return "The value of isMarried is: "+civilStatus
}
let status = returnFunction2(user.isMarried);
console.log(status);